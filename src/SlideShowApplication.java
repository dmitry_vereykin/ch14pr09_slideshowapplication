/**
 * Created by Dmitry Vereykin on 7/29/2015.
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

public class SlideShowApplication extends JApplet {
    private String[] back;
    private int index;
    private ArrayList<File> files;
    private int numberOfImages;
    private File selectedDirectory;
    private JPanel topPanel;
    private JPanel imagePanel;
    private JLabel imageLabel;
    private JButton button;
    private JTextField delayField;
    private JFileChooser fileChooser;
    private JMenuBar menuBar;
    private JMenu fileMenu;
    private JMenuItem exitItem;
    private JMenuItem buttonItem;
    private Timer timer;
    private int delay;

    public void init() {
        setLayout(new BorderLayout());
        buildMenuBar();
        setJMenuBar(menuBar);

        topPanel = new JPanel();
        topPanel.setLayout(new FlowLayout());
        button = new JButton("Start SlideShow");
        button.addActionListener(new TimerListener());
        delayField = new JTextField("", 5);
        JLabel delayLabel = new JLabel("Choose delay in sec");
        topPanel.add(button);
        topPanel.add(delayLabel);
        topPanel.add(delayField);

        imagePanel = new JPanel();
        imagePanel.setLayout(new FlowLayout());
        imageLabel = new JLabel();
        imagePanel.add(imageLabel);

        this.getContentPane().add(topPanel, BorderLayout.CENTER);
        this.getContentPane().add(imagePanel, BorderLayout.SOUTH);

    }

    private void buildMenuBar() {
        menuBar = new JMenuBar();
        buildFileMenu();
        menuBar.add(fileMenu);
    }

    private void buildFileMenu() {
        fileMenu = new JMenu("File");
        fileMenu.setMnemonic(KeyEvent.VK_F);

        buttonItem = new JMenuItem("Open");
        buttonItem.setMnemonic(KeyEvent.VK_O);
        buttonItem.addActionListener(new ButtonListener());

        exitItem = new JMenuItem("Exit");
        exitItem.setMnemonic(KeyEvent.VK_X);
        exitItem.addActionListener(new ExitListener());

        fileMenu.add(buttonItem);
        fileMenu.add(exitItem);
    }

    private class ButtonListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            files = new ArrayList<File>();
            index = 0;
            int fileChooserStatus;
            fileChooser = new JFileChooser();

            if (e.getSource() == buttonItem) {
            fileChooserStatus = fileChooser.showOpenDialog(SlideShowApplication.this);

            if (fileChooserStatus == fileChooser.APPROVE_OPTION) {
                selectedDirectory = fileChooser.getCurrentDirectory();
                File f = new File(String.valueOf(selectedDirectory));
                files = new ArrayList<File>(Arrays.asList(f.listFiles()));
                numberOfImages = files.size();

                for (int n = 0; n < files.size(); n++) {
                    if ((files.get(n).getName()).equals("Thumbs.db")) {
                        files.remove(n);
                    }
                }

            }
            }
        }
    }

    private class TimerListener implements ActionListener {
        public void actionPerformed(ActionEvent e) {
            File selectedFile;
            ImageIcon image;
            String filename;

            if (!selectedDirectory.equals(null)) {
                index++;
                selectedFile = files.get(index);
                filename = selectedFile.getPath();
                image = new ImageIcon(filename);
                imageLabel.setIcon(image);
                imageLabel.setText(null);
                files.remove(index);
            }

            if (e.getSource() == button) {
                delay = Integer.parseInt(delayField.getText());
                    if (timer == null) {
                        timer = new Timer(delay * 1000, this);
                        timer.start();
                    } else
                        timer.restart();
            }
        }
    }

    private class ExitListener implements ActionListener {
        public void actionPerformed(ActionEvent e){
            System.exit(0);
        }
    }


}
